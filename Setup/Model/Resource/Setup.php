<?php

class Doghouse_Setup_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {

    public function getConfigModel($path,$scope='default',$scopeId=0) {

        $data = Mage::getModel('core/config_data')
                ->getCollection()
                ->addFieldToSelect('scope')
                ->addFieldToSelect('path')
                ->addFieldToSelect('scope_id')
                ->addFieldToFilter('path',$path)
                ->addFieldToFilter('scope',$scope)
                ->addFieldToFilter('scope_id',$scopeId)
                ->loadData();

            if ($data->count()) return $data->getFirstItem();

            return Mage::getModel('core/config_data')
                ->setScope($scope)
                ->setScopeId($scopeId)
                ->setPath($path);
        }

    public function setConfigValue($value, $path, $scope='default' ,$scopeId=0 ) {
        return $this->getConfigModel($path,$scope,$scopeId)
            ->setValue($value)
            ->save();
    }

    /**
     * http://gabrielsomoza.com/magento/disabling-a-module-and-its-output-programmatically/
     */
    public function disableModule($moduleName) {
        // Disable the module itself
        $nodePath = "modules/$moduleName/active";
        if (Mage::helper('core/data')->isModuleEnabled($moduleName)) {
            Mage::getConfig()->setNode($nodePath, 'false', true);
        }

        $this->disableModuleOutput($moduleName);
    }

    public function disableModuleOutput($moduleName) {
        // Disable its output as well (which was already loaded)
        $outputPath = "advanced/modules_disable_output/$moduleName";
        if (!Mage::getStoreConfig($outputPath)) {
            Mage::app()->getStore()->setConfig($outputPath, true);
        }
    }

    public function isModuleOutputDisabled($moduleName) {
        $outputPath = "advanced/modules_disable_output/$moduleName";
        return !Mage::getStoreConfig($outputPath);
    }

    public function isModuleDisabled($moduleName) {
        return !Mage::helper('core/data')->isModuleEnabled($moduleName);
    }

    public function enableModuleOutput($moduleName) {
        $outputPath = "advanced/modules_disable_output/$moduleName";
        $this->setConfigValue(0, $outputPath);
    }

}